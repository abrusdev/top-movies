package uz.project.mymovies;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;
import java.util.List;

import uz.project.mymovies.adapters.MovieAdapter;
import uz.project.mymovies.data.MainViewModel;
import uz.project.mymovies.data.rows.FavouriteMovie;
import uz.project.mymovies.data.rows.Movie;

import static uz.project.mymovies.MainActivity.INTENT_ID;

public class FavouriteActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private MovieAdapter adapter;
    private MainViewModel viewModel;

    // NavigationDrawer
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle mToggle;

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        return mToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favourite);

        recyclerView = findViewById(R.id.recyclerViewFavouriteMovies);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        viewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        LiveData<List<FavouriteMovie>> favouriteMovies = viewModel.getFavouriteMovies();
        adapter = new MovieAdapter();

        //Drawer
        drawerLayout = findViewById(R.id.drawerLayoutFavourite);
        mToggle = new ActionBarDrawerToggle(this,drawerLayout,R.string.open,R.string.close);
        drawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final NavigationView navigationView = findViewById(R.id.nav_viewFavPage);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                switch (id) {
                    case R.id.mainPage:
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.favourites:
                        drawerLayout.closeDrawer(GravityCompat.START);
                        break;
                    case R.id.about:
                        Intent intentToAbout = new Intent(getApplicationContext(), AboutActivity.class);
                        startActivity(intentToAbout);
                        break;
                }
                return true;
            }
        });

        adapter.setMovies(this,favouriteMovies);
        recyclerView.setAdapter(adapter);

        favouriteMovies.observe(this, new Observer<List<FavouriteMovie>>() {
            @Override
            public void onChanged(List<FavouriteMovie> favouriteMovies) {
                loadFavouriteMovies(favouriteMovies);
            }

        });

        adapter.setOnPosterClickListener(new MovieAdapter.onPosterClickListener() {
            @Override
            public void onPosterClick(int position) {
                Movie movie = adapter.getMovies().get(position);
                Intent intent = new Intent(FavouriteActivity.this, DetailActivity.class);
                intent.putExtra(INTENT_ID, movie.getId());
                startActivity(intent);
            }
        });
    }

    public void loadFavouriteMovies(List<FavouriteMovie> list) {
        if (list != null) {
            List<Movie> movies = new ArrayList<>();
            movies.addAll(list);
            adapter.setMovies(movies);
        }
    }
}
