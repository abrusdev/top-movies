package uz.project.mymovies;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.loader.app.LoaderManager;
import androidx.loader.content.Loader;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.navigation.NavigationView;

import org.json.JSONObject;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import uz.project.mymovies.adapters.MovieAdapter;
import uz.project.mymovies.data.MainViewModel;
import uz.project.mymovies.data.rows.Movie;
import uz.project.mymovies.utils.JSONUtils;
import uz.project.mymovies.utils.NetworkUtils;

public class MainActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<JSONObject> {

    ArrayList<Movie> movies;
    private LoaderManager loaderManager;
    public static final String INTENT_ID = "MAIN_INTENT_ID";
    private static final String PREF_SORT_KEY = "PREF_SWITCH_TYPE";
    private static final int LOADER_ID = 133;

    private static int page = 1;
    private static int METHOD_SORT;
    private static boolean isLoading = false;

    private MovieAdapter movieAdapter;
    private ConstraintLayout progressbarView;
    private RelativeLayout progressbarBG;
    private RelativeLayout progressBar;
    RecyclerView recyclerView;
    SharedPreferences preferences;
    private MainViewModel viewModel;

    // NavigationDrawer
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle mToggle;
    private Button mostPopularBtn, topRatedBtn;

    /**
     * Main operations of AppCompatActivity
     **/

    @Override
    protected void onPause() {
        super.onPause();
        preferences.edit().putBoolean(PREF_SORT_KEY, METHOD_SORT == 1).apply();
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        return mToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loaderManager = LoaderManager.getInstance(this);
        viewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        LiveData<List<Movie>> moviesFromLiveData = viewModel.getMovies();

        RecyclerView recyclerView = findViewById(R.id.recyclerView_posters);
        progressbarView = findViewById(R.id.progressMainPage);
        progressbarBG = findViewById(R.id.progressViewBG);
        progressBar = findViewById(R.id.progressBar);

        mostPopularBtn = findViewById(R.id.mostPopular_btn);
        topRatedBtn = findViewById(R.id.topRated_btn);

        //Drawer
        drawerLayout = findViewById(R.id.drawerLayoutMain);
        mToggle = new ActionBarDrawerToggle(this,drawerLayout,R.string.open,R.string.close);
        drawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                switch (id) {
                    case R.id.mainPage:
                        drawerLayout.closeDrawer(GravityCompat.START);
                        break;
                    case R.id.favourites:
                        Intent intentToFavourite = new Intent(getApplicationContext(), FavouriteActivity.class);
                        startActivity(intentToFavourite);
                        break;
                    case R.id.about:
                        Intent intent = new Intent(getApplicationContext(), AboutActivity.class);
                        startActivity(intent);
                        break;
                }
                return true;
            }
        });

        // get variable from Saved Preferences
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        if (preferences.contains(PREF_SORT_KEY)) {
            setColorSwitchAndTexts();
        }

        // set Adapter
        recyclerView.setLayoutManager(new GridLayoutManager(this, getColumnCount()));
        JSONObject jsonObject = NetworkUtils.getJSONFromNetwork(NetworkUtils.POPULARITY, 1);
        movies = JSONUtils.getMoviesFromJson(jsonObject);
        movieAdapter = new MovieAdapter();
        movieAdapter.setMovies(movies);
        recyclerView.setAdapter(movieAdapter);

        moviesFromLiveData.observe(this, new Observer<List<Movie>>() {
            @Override
            public void onChanged(List<Movie> movies) {
                if (page == 1) {
                    movieAdapter.setMovies(movies);
                }
            }
        });
//
//        switchSort.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                page = 1;
//                setMethodOfSort(isChecked);
//                downloadData(METHOD_SORT, page);
//            }
//        });

        movieAdapter.setOnPosterClickListener(new MovieAdapter.onPosterClickListener() {
            @Override
            public void onPosterClick(int position) {
                Movie movie = movieAdapter.getMovies().get(position);
                Intent intent = new Intent(MainActivity.this, DetailActivity.class);
                intent.putExtra(INTENT_ID, movie.getId());
                startActivity(intent);
            }
        });

        movieAdapter.setOnReachEndListener(new MovieAdapter.onReachEndListener() {
            @Override
            public void onReachEnd() {
                if (!isLoading) {
                    downloadData(METHOD_SORT, page);
                }
            }
        });
    }


    /**
     * View buttons onClick
     **/

    public void onClickSetTopRated(View view) {
        page = 1;
        setMethodOfSort(true);
    }

    public void onClickSetPopularity(View view) {
        page = 1;
        setMethodOfSort(false);
    }


    /**
     * LoaderManager Callbacks
     **/

    @NonNull
    @Override
    public Loader<JSONObject> onCreateLoader(int id, @Nullable Bundle args) {
        NetworkUtils.JSONLoader jsonLoader = new NetworkUtils.JSONLoader(this, args);
        jsonLoader.setOnStartLoadingListener(new NetworkUtils.JSONLoader.OnStartLoadingListener() {
            @Override
            public void onStartLoading() {
                mostPopularBtn.setClickable(false);
                topRatedBtn.setClickable(false);
                progressbarView.setVisibility(View.VISIBLE);
                progressbarBG.animate().scaleX(40f).scaleY(40f).alpha(0.4f).setDuration(500);
                isLoading = true;
            }
        });
        return jsonLoader;
    }

    @Override
    public void onLoadFinished(@NonNull Loader<JSONObject> loader, JSONObject data) {
        ArrayList<Movie> movies = JSONUtils.getMoviesFromJson(data);
        if (!movies.isEmpty()) {
            if (page == 1) {
                viewModel.deleteAllMovies();
                movieAdapter.clear();
            }
            for (Movie movie : movies) {
                viewModel.insertMovie(movie);
            }
            movieAdapter.addMovies(movies);
            page++;
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(View.GONE);
            }
        }, 500);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                progressbarView.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
                mostPopularBtn.setClickable(true);
                topRatedBtn.setClickable(true);
            }
        }, 1500);
        progressbarBG.animate().scaleX(1f).scaleY(1f).alpha(0).setDuration(1500);
        isLoading = false;
        loaderManager.destroyLoader(LOADER_ID);
    }

    @Override
    public void onLoaderReset(@NonNull Loader<JSONObject> loader) {

    }


    /**
     * Functions
     **/

    private void setMethodOfSort(boolean isTopRated) {
        if (isTopRated) {
            METHOD_SORT = NetworkUtils.TOP_RATED;
            topRatedBtn.setTextColor(getResources().getColor(R.color.color_grey));
            mostPopularBtn.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                topRatedBtn.setTextColor(getResources().getColor(R.color.color_white));
                topRatedBtn.setBackgroundTintList(getResources().getColorStateList(R.color.colorPrimaryDark));
                mostPopularBtn.setBackgroundTintList(getResources().getColorStateList(R.color.color_white));
            }

        } else {
            METHOD_SORT = NetworkUtils.POPULARITY;
            mostPopularBtn.setTextColor(getResources().getColor(R.color.color_grey));
            topRatedBtn.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mostPopularBtn.setTextColor(getResources().getColor(R.color.color_white));
                mostPopularBtn.setBackgroundTintList(getResources().getColorStateList(R.color.colorPrimaryDark));
                topRatedBtn.setBackgroundTintList(getResources().getColorStateList(R.color.color_white));
            }

        }
        downloadData(METHOD_SORT, page);
    }

    private void downloadData(int methodOfSort, int page) {
        URL url = NetworkUtils.buildURL(methodOfSort, page);
        Bundle bundle = new Bundle();
        bundle.putString("url", url.toString());
        loaderManager.restartLoader(LOADER_ID, bundle, this);
    }

    private int getColumnCount() {
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int width = (int) (displayMetrics.widthPixels / displayMetrics.density);
        return Math.max(width / 185, 2);
    }

    private void setColorSwitchAndTexts() {
        boolean isTopRated = preferences.getBoolean(PREF_SORT_KEY, false);
        if (isTopRated) {
            METHOD_SORT = NetworkUtils.TOP_RATED;
            topRatedBtn.setTextColor(getResources().getColor(R.color.color_grey));
            mostPopularBtn.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                topRatedBtn.setTextColor(getResources().getColor(R.color.color_white));
                topRatedBtn.setBackgroundTintList(getResources().getColorStateList(R.color.colorPrimaryDark));
                mostPopularBtn.setBackgroundTintList(getResources().getColorStateList(R.color.color_white));
            }

        } else {
            METHOD_SORT = NetworkUtils.POPULARITY;
            mostPopularBtn.setTextColor(getResources().getColor(R.color.color_grey));
            topRatedBtn.setTextColor(getResources().getColor(R.color.colorPrimaryDark));
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mostPopularBtn.setTextColor(getResources().getColor(R.color.color_white));
                mostPopularBtn.setBackgroundTintList(getResources().getColorStateList(R.color.colorPrimaryDark));
                topRatedBtn.setBackgroundTintList(getResources().getColorStateList(R.color.color_white));
            }

        }
    }

    private void setVisibleProgressBar(boolean isLoaded){
        if (!isLoaded){

        } else {

        }
    }

}
