package uz.project.mymovies;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.ArrayList;

import uz.project.mymovies.adapters.ReviewAdapter;
import uz.project.mymovies.adapters.TrailerAdapter;
import uz.project.mymovies.data.MainViewModel;
import uz.project.mymovies.data.rows.FavouriteMovie;
import uz.project.mymovies.data.rows.Movie;
import uz.project.mymovies.data.rows.Review;
import uz.project.mymovies.data.rows.Trailer;
import uz.project.mymovies.utils.JSONUtils;
import uz.project.mymovies.utils.NetworkUtils;

public class DetailActivity extends AppCompatActivity {

    private ImageView bigPoster, ic_star;
    private TextView title_txt, originalTitle_txt, rating_txt, releaseDate_txt, description_txt;
    private RelativeLayout bg_star;

    private RecyclerView recyclerViewTrailers, recyclerViewReviews;
    private ReviewAdapter reviewAdapter;
    private TrailerAdapter trailerAdapter;
    private ImageView trailerIcon;
    private ImageView reviewIcon;

    private int id;
    private MainViewModel viewModel;
    private Movie movie;

    private ScrollView scrollViewDetail;

    ArrayList<Trailer> trailers;
    ArrayList<Review> reviews;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        bigPoster = findViewById(R.id.bigPoster_img);
        title_txt = findViewById(R.id.title_txt);
        originalTitle_txt = findViewById(R.id.originalTitle_txt);
        rating_txt = findViewById(R.id.rating_txt);
        releaseDate_txt = findViewById(R.id.releaseDate_txt);
        description_txt = findViewById(R.id.description_txt);
        trailerIcon = findViewById(R.id.trailer_more_icon);
        scrollViewDetail = findViewById(R.id.scrollViewDetail);
        reviewIcon = findViewById(R.id.review_more_icon);


        bg_star = findViewById(R.id.bg_start_view);
        ic_star = findViewById(R.id.ic_start_img);

        viewModel = ViewModelProviders.of(this).get(MainViewModel.class);

        Intent intent = getIntent();
        if (intent != null && intent.hasExtra(MainActivity.INTENT_ID)) {
            id = intent.getIntExtra(MainActivity.INTENT_ID, -1);
        } else {
            finish();
        }

        FavouriteMovie favouriteMovie = viewModel.getFavouriteMovieById(id);
        if (favouriteMovie != null){
            ic_star.setBackgroundResource(R.drawable.ic_star);
        }

        movie = viewModel.getMovieById(id);
        Picasso.get().load(movie.getBigPosterPath()).placeholder(R.mipmap.no_image).into(bigPoster);
        title_txt.setText(movie.getTitle());
        originalTitle_txt.setText(movie.getOriginalTitle());
        rating_txt.setText(Double.toString(movie.getVoteAvarage()));
        releaseDate_txt.setText(movie.getReleaseDate());
        description_txt.setText(movie.getOverview());

        recyclerViewTrailers = findViewById(R.id.recyclerViewTrailers);
        recyclerViewReviews = findViewById(R.id.recyclerViewReviews);
        reviewAdapter = new ReviewAdapter();
        trailerAdapter = new TrailerAdapter();
        trailerAdapter.setOnTrailerClickListener(new TrailerAdapter.onTrailerClickListener() {
            @Override
            public void onTrailerClick(String url) {
                Intent intentToTrailer = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(intentToTrailer);
            }
        });
        recyclerViewReviews.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewTrailers.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewReviews.setAdapter(reviewAdapter);
        recyclerViewTrailers.setAdapter(trailerAdapter);

        JSONObject jsonObjectTrailers = NetworkUtils.getJSONForVideos(movie.getId());
        JSONObject jsonObjectReviews = NetworkUtils.getJSONForReviews(movie.getId());
        trailers = JSONUtils.getTrailersFromJson(jsonObjectTrailers);
        reviews = JSONUtils.getReviewsFromJson(jsonObjectReviews);
        reviewAdapter.setReviews(reviews);
        trailerAdapter.setTrailers(trailers);
    }

    public void onClickAddFavourite(View view) {
        FavouriteMovie favouriteMovie = viewModel.getFavouriteMovieById(id);
        if (favouriteMovie == null){
            viewModel.insertFavouriteMovie(new FavouriteMovie(movie));
            bg_star.animate().alphaBy(1f).alpha(0).scaleX(0).scaleY(0).setDuration(300);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    ic_star.setBackgroundResource(R.drawable.ic_star);
                    bg_star.animate().alpha(1).scaleY(1).scaleX(1).setDuration(300);
                }
            },300);
            Toast.makeText(this, R.string.added_to_favourites, Toast.LENGTH_SHORT).show();
        } else {
            viewModel.deleteFavouriteMovie(new FavouriteMovie(movie));
            bg_star.animate().alphaBy(1f).alpha(0).scaleX(0).scaleY(0).setDuration(300);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    ic_star.setBackgroundResource(R.drawable.ic_star_border);
                    bg_star.animate().alpha(1).scaleY(1).scaleX(1).setDuration(300);
                }
            },300);
            Toast.makeText(this, R.string.removed_from_favourites, Toast.LENGTH_SHORT).show();
        }

    }

    public void onClickshowMoreTrailers(View view) {

        if (recyclerViewTrailers.getVisibility() == View.VISIBLE){
            trailerIcon.animate().rotationBy(180).rotation(0).setDuration(500);
            recyclerViewTrailers.setVisibility(View.GONE);
        } else {
            trailerIcon.animate().rotationBy(0).rotation(180).setDuration(500);
            recyclerViewTrailers.setVisibility(View.VISIBLE);

        }

    }

    public void onClickshowMoreReviews(View view) {
        if (recyclerViewReviews.getVisibility() == View.VISIBLE){
            reviewIcon.animate().rotationBy(180).rotation(0).setDuration(500);
            recyclerViewReviews.setVisibility(View.GONE);
        } else {
            reviewIcon.animate().rotationBy(0).rotation(180).setDuration(500);
            recyclerViewReviews.setVisibility(View.VISIBLE);
        }

    }
}
