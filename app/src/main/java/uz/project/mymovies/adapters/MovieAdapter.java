package uz.project.mymovies.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import uz.project.mymovies.R;
import uz.project.mymovies.data.rows.FavouriteMovie;
import uz.project.mymovies.data.rows.Movie;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MoviewViewHolder> {

    private List<Movie> movies;
    private static onPosterClickListener onPosterClickListener;
    private static onReachEndListener onReachEndListener ;

    public interface onPosterClickListener{
        void onPosterClick(int position);
    }

    public interface onReachEndListener{
        void onReachEnd();
    }

    public MovieAdapter() {
        this.movies = new ArrayList<>();
    }

    public void setOnPosterClickListener(MovieAdapter.onPosterClickListener onPosterClickListener) {
        this.onPosterClickListener = onPosterClickListener;
    }

    public void setOnReachEndListener(MovieAdapter.onReachEndListener onReachEndListener) {
        MovieAdapter.onReachEndListener = onReachEndListener;
    }

    @NonNull
    @Override
    public MoviewViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.movie_item,parent,false);
        return new MoviewViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MoviewViewHolder holder, int position) {
        Movie movie = movies.get(position);
        holder.votesCount.setText(Integer.toString(movie.getVoteCount()));
        holder.releaseDate.setText(movie.getReleaseDate().toString() + " ");
        Picasso.get().load(movie.getPosterPath()).placeholder(R.mipmap.no_image).into(holder.imageViewSmallPoster);
        if (movies.size() >= 20 && position == movies.size()-4 && onReachEndListener != null){
            onReachEndListener.onReachEnd();
        }
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    static class MoviewViewHolder extends RecyclerView.ViewHolder{

        private ImageView imageViewSmallPoster;
        private TextView votesCount, releaseDate;

        public MoviewViewHolder(@NonNull View itemView) {
            super(itemView);
            imageViewSmallPoster = itemView.findViewById(R.id.imageViewSmallPoster);
            votesCount = itemView.findViewById(R.id.votesCount);
            releaseDate = itemView.findViewById(R.id.releaseDate);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (onPosterClickListener != null){
                        onPosterClickListener.onPosterClick(getAdapterPosition());
                    }
                }
            });
        }
    }

    public List<Movie> getMovies() {
        return movies;
    }

    public void addMovies(List<Movie> movies){
        this.movies.addAll(movies);
        notifyDataSetChanged();
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
        notifyDataSetChanged();
    }

    public void setMovies(LifecycleOwner context, LiveData<List<FavouriteMovie>> movies){
        movies.observe(context, new Observer<List<FavouriteMovie>>() {
            @Override
            public void onChanged(List<FavouriteMovie> movies) {
                List<Movie> result = new ArrayList<>();
                result.addAll(movies);
                setMovies(result);
            }
        });
    }

    public void clear(){
        this.movies.clear();
        notifyDataSetChanged();
    }
}
