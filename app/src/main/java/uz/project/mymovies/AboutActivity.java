package uz.project.mymovies;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import com.google.android.material.navigation.NavigationView;

public class AboutActivity extends AppCompatActivity {

    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle mToggle;
    private LinearLayout linkToInstagram, linkToTelegram, linkToGmail;

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        return mToggle.onOptionsItemSelected(item) || super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        drawerLayout = findViewById(R.id.drawerLayoutAbout);
        linkToTelegram = findViewById(R.id.aboutLinkToTelegram);
        linkToInstagram = findViewById(R.id.aboutLinkToInstagram);
        linkToGmail = findViewById(R.id.aboutLinkToGmail);

        //Drawer
        mToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.open, R.string.close);
        drawerLayout.addDrawerListener(mToggle);
        mToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                switch (id) {
                    case R.id.mainPage:
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.favourites:
                        Intent intentToFavourite = new Intent(getApplicationContext(), FavouriteActivity.class);
                        startActivity(intentToFavourite);
                        break;
                    case R.id.about:
                        drawerLayout.closeDrawer(GravityCompat.START);
                        break;
                }
                return true;
            }
        });

        linkToTelegram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent likeIng = new Intent(Intent.ACTION_VIEW,Uri.parse("tg://abrusdev"));
                startActivity(likeIng);
            }
        });
        linkToInstagram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Uri uri = Uri.parse("https://www.instagram.com/abrusdev/").buildUpon().build();
                Intent likeIng = new Intent(Intent.ACTION_VIEW,uri);
                startActivity(likeIng);
            }
        });
        linkToGmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(AboutActivity.this, "Our gmail account: abrusdev@gmail.com", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
