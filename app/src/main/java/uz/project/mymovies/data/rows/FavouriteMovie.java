package uz.project.mymovies.data.rows;

import androidx.room.Entity;
import androidx.room.Ignore;


@Entity(tableName = "favourite_movies")
public class FavouriteMovie extends Movie {

    public FavouriteMovie(int uniqueID, int id, int voteCount, String title, String originalTitle, String overview, String posterPath, String bigPosterPath, String backdropPath, double voteAvarage, String releaseDate) {
        super(uniqueID, id, voteCount, title, originalTitle, overview, posterPath, bigPosterPath, backdropPath, voteAvarage, releaseDate);
    }

    @Ignore
    public FavouriteMovie(Movie movie){
        super(movie.getUniqueID(), movie.getId(), movie.getVoteCount(), movie.getTitle(), movie.getOriginalTitle(), movie.getOverview(), movie.getPosterPath(), movie.getBigPosterPath(), movie.getBackdropPath(), movie.getVoteAvarage(), movie.getReleaseDate());
    }

}
